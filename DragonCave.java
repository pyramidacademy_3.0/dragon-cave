package com.project;

import java.util.Scanner;

public class  DragonCave {
    public static void main(String[] args) {
      int ChoiceCave;
      Scanner sca = new Scanner(System.in);
        System.out.println("You are in a land full of dragons. In front of you,\n" +
                  "you see two caves. In one cave, the dragon is friendly\n" +
                   "and will share his treasure with you. The other dragon\n" +
                  "is greedy and hungry and will eat you on sight.");
          System.out.println("Which cave will you go into? (1 or 2)");

          ChoiceCave = sca.nextInt();
          if(ChoiceCave == 1){
              System.out.println("\n" +
                     "You approach the first cave...\n" +
                       "It is dark and spooky...\n" +
                        "A large dragon jumps out in front of you! He opens his jaws and...\n" +
                        "Gobbles you down in one bite!");
                      return;
          }
          else {
              System.out.println("\nYou approach the cave..\n"+
                                      "It is having light and is clean..\n"+
                                      "A friendly dragon comes in front of you! He plays with you and...\n"+
                                          "Shares his treasures!");
          }


    }
    }

